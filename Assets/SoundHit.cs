﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundHit : MonoBehaviour {
	
	public AudioSource audioSource;
	
	public List<AudioClip> sounds = new List<AudioClip>();
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	void OnCollisionEnter(Collision coll) 
	{
		
		if (coll.gameObject.tag != "shoe")
			return;
		
		if (Mathf.Abs(coll.impactForceSum.x + coll.impactForceSum.y + coll.impactForceSum.z) < 3f)
			return;
		
		if (audioSource.isPlaying)
			return;
		
		int i = Random.Range(0,sounds.Count);
		
		while(i == sounds.Count)
		{
			i = Random.Range(0,sounds.Count);
		}
		
		//play damage sound
		audioSource.PlayOneShot(sounds[i]);
		
		
	}
}
